class AddImageToFoodRecipe < ActiveRecord::Migration[6.0]
  def change
    add_column :food_recipes, :image, :string
  end
end
