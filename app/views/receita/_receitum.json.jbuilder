json.extract! receitum, :id, :title, :description, :created_at, :updated_at
json.url receitum_url(receitum, format: :json)
