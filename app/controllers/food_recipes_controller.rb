class FoodRecipesController < ApplicationController
  before_action :set_food_recipe, only: [:show, :edit, :update, :destroy]

  # GET /food_recipes
  # GET /food_recipes.json
  def index
    @food_recipes = FoodRecipe.all
  end

  def search
    @food_recipes = FoodRecipe.search(params[:q])
  end

  # GET /food_recipes/1
  # GET /food_recipes/1.json
  def show
  end

  # GET /food_recipes/new
  def new
    @food_recipe = FoodRecipe.new
  end

  # GET /food_recipes/1/edit
  def edit
  end

  # POST /food_recipes
  # POST /food_recipes.json
  def create
    @food_recipe = FoodRecipe.new(food_recipe_params)

    respond_to do |format|
      if @food_recipe.save
        format.html { redirect_to @food_recipe, notice: 'Food recipe was successfully created.' }
        format.json { render :show, status: :created, location: @food_recipe }
      else
        format.html { render :new }
        format.json { render json: @food_recipe.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /food_recipes/1
  # PATCH/PUT /food_recipes/1.json
  def update
    respond_to do |format|
      if @food_recipe.update(food_recipe_params)
        format.html { redirect_to @food_recipe, notice: 'Food recipe was successfully updated.' }
        format.json { render :show, status: :ok, location: @food_recipe }
      else
        format.html { render :edit }
        format.json { render json: @food_recipe.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /food_recipes/1
  # DELETE /food_recipes/1.json
  def destroy
    @food_recipe.destroy
    respond_to do |format|
      format.html { redirect_to food_recipes_url, notice: 'Food recipe was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_food_recipe
      @food_recipe = FoodRecipe.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def food_recipe_params
      params.require(:food_recipe).permit(:remove_image, :image, :title, :description, :ingredient, :step)
    end
end
