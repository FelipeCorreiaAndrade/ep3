# EP3 Recipe App

Trabalho 3 da disciplina OO, site de receitas!

## Sobre o projeto

Esse projeto foi feito em Ruby, mas especificamente ruby on rails.

### Prerequisitos

Primeiramente teremos que ter na máquina a versão do ruby e do rails.

As vesões necessarias para a funcionalidade do programa são

ruby 2.6.2

rails 6.0.1

### Instalando

Agora para rodar o programa e necessario utilizar o comando git clone para ter o repositorio em sua máquina.

```
git clone https://gitlab.com/FelipeCorreiaAndrade/ep3.git
```

Agora já possui um repositorio local.

Precisamos tambem das gems e pacotes que exixtem no projeto, então e necessario que entre na pasta onde esta o repositorio, pelo terminal, e executar o seguinte comando:

```
bundle install
```

Com isso, quase tudo pronto! Basta rodar o comando rails s para abrir o server e entrar pelo navegador no link:

* (http://localhost:3000/)

## Autores

* **Felipe Correia** 

* **Filipe Oliveira**

Participantes do projeto: [contributors](https://gitlab.com/FelipeCorreiaAndrade/ep3/-/graphs/master)

